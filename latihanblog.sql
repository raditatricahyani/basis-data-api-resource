-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Sep 2020 pada 08.53
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `latihanblog`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `created_at`, `updated_at`, `deleted_at`, `category`) VALUES
(1, '7 Makanan Tradisional Khas Jawa yang Cocok Disajikan saat HUT RI ke-75', 'Jakarta - Makanan tradisional khas Jawa pilihannya sangat beragam. Semua makanan ini memiliki makna spesial sehingga jadi sajian populer saat perayaan-perayaan tertentu, termasuk pada Hari Kemerdekaan HUT RI yang ke-75 ini.', NULL, NULL, NULL, 'HUT RI Ke-75'),
(2, 'test', 'test', NULL, NULL, NULL, 'test'),
(3, 'Mr.', 'Sapiente sint omnis aliquam sunt iure. Sapiente quia quas eveniet quibusdam ab minima. Dolorum quis distinctio excepturi dignissimos veniam fuga.', '2020-09-16 03:01:50', '2020-09-16 03:01:50', NULL, 'Stock Broker'),
(4, 'Prof.', 'Qui eveniet voluptas nemo placeat fugiat. Consequatur sed beatae nisi dicta quis odit. Neque qui inventore enim amet ut. Dolores ratione vero animi inventore.', '2020-09-16 03:01:50', '2020-09-16 03:01:50', NULL, 'Marine Engineer'),
(5, 'Dr.', 'Doloremque et qui aliquam qui voluptatem et nobis omnis. Quibusdam maiores voluptatem et dolores impedit neque culpa.', '2020-09-16 03:01:50', '2020-09-16 03:01:50', NULL, 'Septic Tank Servicer'),
(6, 'Miss', 'Ut et maxime quis sapiente eos. Suscipit quia repudiandae impedit voluptatem itaque ut. Dolorem a animi impedit repudiandae autem.', '2020-09-16 03:01:50', '2020-09-16 03:01:50', NULL, 'Counsil'),
(7, 'Prof.', 'Provident temporibus earum harum eos qui. Commodi voluptate magni et doloremque commodi adipisci a. Distinctio provident nihil omnis aliquid voluptas culpa et necessitatibus. Perspiciatis accusamus rerum maxime sit.', '2020-09-16 03:01:50', '2020-09-16 03:01:50', NULL, 'ccc'),
(8, 'Prof.', 'Aut rerum neque perspiciatis quidem in. Non ullam voluptates totam numquam dolor vero animi. Ab error autem possimus rem. Odit possimus adipisci natus voluptas vel.', '2020-09-16 03:01:50', '2020-09-16 03:01:50', NULL, 'Fish Hatchery Manager'),
(9, 'Dr.', 'Animi rem cum error at. Quos officia placeat ratione molestiae rerum beatae dolores. Minus eaque sed adipisci accusamus adipisci asperiores molestiae. Est iusto doloremque optio corporis ut earum enim.', '2020-09-16 03:01:50', '2020-09-16 03:01:50', NULL, 'Education Teacher'),
(10, 'Ms.', 'Quod repellat non aliquid nemo sunt. Tempore cumque qui asperiores est consectetur. Veniam in fugiat corporis id necessitatibus dolorum. Ut illum earum fugiat. Vitae assumenda soluta quia rerum nemo laborum qui.', '2020-09-16 03:01:50', '2020-09-16 03:01:50', NULL, 'Educational Counselor OR Vocationall Counselor'),
(11, 'Mrs.', 'Ipsam veniam possimus ut nihil modi non nulla voluptate. Fuga sunt quas deleniti. Possimus eos porro omnis iste expedita ipsam non.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Elementary School Teacher'),
(12, 'Dr.', 'Nulla a harum et provident. Et nihil eum ut iure est aspernatur.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Postmasters'),
(13, 'Dr.', 'Magni dolorem iste iusto. Saepe nisi deleniti excepturi ea nam asperiores quia aliquam. Consectetur ex neque reprehenderit vel cupiditate quo.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Industrial Production Manager'),
(14, 'Mrs.', 'Sequi nisi harum in et quia. Maxime expedita sed enim ad et suscipit pariatur. Blanditiis deleniti corporis totam deleniti non unde.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Courier'),
(15, 'Mr.', 'Velit eum nihil eligendi modi perferendis modi non. Illum iste alias ut reiciendis voluptas. Ipsum voluptas blanditiis qui optio harum a fugiat quaerat. Natus delectus cum rem. Ut reiciendis eligendi ex.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Set Designer'),
(16, 'Dr.', 'Dolores nobis eos quae voluptas quidem iure eum quis. Porro numquam laborum eius esse. Hic dicta assumenda facere voluptatem dolorem.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Technical Program Manager'),
(17, 'Prof.', 'Eos magnam dolores optio iusto. Nisi sunt et facere id. Eaque in consequatur ea aut.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Talent Director'),
(18, 'Prof.', 'Quia aut ex consequatur dolore cumque non. Non omnis sunt rem ut repellat. Qui nobis fugit consequatur qui est sed.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Oil and gas Operator'),
(19, 'Ms.', 'Voluptatum quia voluptatem laborum quas eum quam. Rerum rerum voluptatem aut in eaque. Ullam saepe et blanditiis a non voluptate. Corporis laboriosam doloribus eveniet repudiandae qui ipsum.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Assessor'),
(20, 'Dr.', 'Voluptas molestias quia qui occaecati ullam ex. Enim recusandae eveniet inventore sapiente harum dolorem. Saepe tenetur corporis laudantium amet perferendis neque sit.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Athletic Trainer'),
(21, 'Ms.', 'Blanditiis dolorem qui sed nam minus ut commodi. Vel nulla consequatur eius ut quidem. Ratione ut voluptas sit porro optio voluptatum. Consequatur et minima officia dolores repellendus non dolor.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Forest and Conservation Technician'),
(22, 'Ms.', 'Qui dolor velit vitae. Magnam quam eum ipsam assumenda aut sunt. Explicabo et quis autem inventore est occaecati quos. Molestiae tempora officia voluptatem inventore possimus itaque.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Aircraft Assembler'),
(23, 'Miss', 'Ut necessitatibus molestias sint sed odit. Repellat est inventore debitis ratione tempora. Fugit aperiam eum illum dignissimos nemo quia nemo.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Health Practitioner'),
(24, 'Mr.', 'Neque qui tempora totam qui aut quod. Aut voluptates repellendus ut sint. Ducimus omnis provident similique laboriosam suscipit. Possimus minima dolorem qui dolorum delectus dignissimos a quidem.', '2020-09-16 03:01:51', '2020-09-16 03:01:51', NULL, 'Forensic Science Technician'),
(25, 'Prof.', 'Blanditiis illum perspiciatis ut praesentium. Molestiae non consequatur dolore a inventore. Minus magnam labore ad officiis fugiat velit ut.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Police and Sheriffs Patrol Officer'),
(26, 'Mr.', 'Et doloribus qui possimus rem minus voluptatibus similique dolorem. Doloremque provident est rerum saepe molestias repellat sequi laudantium. Optio voluptas omnis et consequuntur cumque velit.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Grinding Machine Operator'),
(27, 'Mr.', 'Consequatur assumenda odio eius sit. Animi repellendus in nam quisquam dolorem tempore. Autem sit accusantium distinctio eius sit tempora cum.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Telephone Station Installer and Repairer'),
(28, 'Dr.', 'Ut illo et dolores autem officiis fugiat sit molestiae. Quo consequatur quia dolorum et. Qui debitis optio id sapiente.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Rail Yard Engineer'),
(29, 'Ms.', 'Dicta et veniam est veritatis. Iste ut quidem distinctio accusantium id sint tempore. Ex omnis facilis omnis ullam amet id vero. Commodi molestias dolorum ea consectetur sint. Rerum quia repellendus voluptates dolores.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Library Science Teacher'),
(30, 'Mr.', 'Officiis reprehenderit consequatur hic. Praesentium voluptatem nulla sit.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Media and Communication Worker'),
(31, 'Miss', 'Numquam aut amet similique a. Quod cum qui enim eum velit quibusdam aut. Dolores itaque ipsa praesentium odit. Non voluptatibus eveniet suscipit dolor et corrupti provident.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Electromechanical Equipment Assembler'),
(32, 'Prof.', 'Laudantium enim non magni est eaque qui dignissimos. Officiis voluptatum ea et eaque quod aspernatur. Harum autem sit ipsa vel odit ut quia qui. Excepturi vero doloribus eligendi cupiditate.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Library Worker'),
(33, 'Dr.', 'Ab voluptatem magni blanditiis omnis. Commodi voluptatem delectus sequi fugit maxime molestias. Recusandae et sint culpa totam enim eius autem. Et laboriosam deleniti quasi dolorum rerum.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Vending Machine Servicer'),
(34, 'Dr.', 'Dolorem officia fugit ducimus enim aut amet consequuntur. Consequatur quae consequatur quia rerum omnis. Quia aut nisi natus rerum et asperiores aut. Quis sed deserunt recusandae delectus.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Sys Admin'),
(35, 'Miss', 'Quaerat eaque eum vitae magni veritatis. Modi voluptatem officia tempore adipisci ut velit distinctio. Ipsa officiis et nihil commodi temporibus pariatur.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Floor Finisher'),
(36, 'Mr.', 'Culpa eius enim consequatur voluptas est. Amet necessitatibus perspiciatis voluptatem saepe. Aut officiis maxime ducimus eum aut.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Mechanical Door Repairer'),
(37, 'Mrs.', 'Impedit autem quia eum aut et. Ullam velit et quia expedita. Inventore tenetur praesentium sapiente rerum asperiores est.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Excavating Machine Operator'),
(38, 'Prof.', 'Qui laborum hic accusantium ex iste repellendus nihil. Suscipit natus at temporibus velit perspiciatis ut. Quisquam sint at consectetur qui.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Logging Worker'),
(39, 'Ms.', 'Culpa nihil odit culpa. Libero mollitia odio odio minus. Inventore et eius est magni harum omnis. Hic est est tempora.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Industrial Production Manager'),
(40, 'Prof.', 'Voluptatem consequatur est dolores delectus suscipit ducimus non. Minus ad et deleniti molestiae accusamus voluptas dolor. Rerum consequuntur consequatur architecto sequi.', '2020-09-16 03:01:52', '2020-09-16 03:01:52', NULL, 'Lifeguard'),
(41, 'Mrs.', 'Quo reiciendis debitis ex asperiores eos maxime. Porro molestiae et aliquam ducimus. Quaerat consequatur fugiat perspiciatis aut voluptas quis. Maiores modi exercitationem quas optio autem exercitationem.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Numerical Tool Programmer OR Process Control Programmer'),
(42, 'Prof.', 'Beatae et omnis minima accusamus sit dolores. Omnis laudantium sit id vitae. Rerum aliquam quasi fugiat minus at. Voluptatem non dignissimos perferendis accusamus et eos magnam.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Data Entry Operator'),
(43, 'Dr.', 'Ratione molestias quasi velit minima autem alias. Sequi dolores neque quis autem quaerat dolorem. Cumque modi dolorum laboriosam animi aut eum.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Loan Interviewer'),
(44, 'Mrs.', 'Sed mollitia omnis molestiae porro. Rem consequatur et libero non vel reiciendis. Sunt distinctio cupiditate eos eos.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Instrument Sales Representative'),
(45, 'Mrs.', 'Quis repudiandae sunt perferendis numquam rerum. Quis sed quis voluptatum animi fugit eius. Eos quia aut voluptatum id est odit et a. Et consectetur alias blanditiis et voluptas ipsa dolore.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Bicycle Repairer'),
(46, 'Dr.', 'Porro perferendis aut error. Et voluptatem eos possimus repellat eveniet. Itaque dignissimos assumenda vel porro. Sapiente veritatis consectetur perferendis quos officia facilis cupiditate.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Manager of Food Preparation'),
(47, 'Dr.', 'Ea nostrum aut est aut facere fugiat harum est. Omnis laboriosam maxime magni. Ab qui id doloribus mollitia.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Computer Programmer'),
(48, 'Miss', 'Perferendis voluptatem nisi veritatis aspernatur et. Temporibus voluptatibus dolore eligendi et autem debitis illum ad. Necessitatibus tempora facilis repellendus nam repellat voluptas nesciunt. Explicabo quo porro sed quae.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Carpenter Assembler and Repairer'),
(49, 'Prof.', 'Dolores eius odio molestias qui eos. Sit voluptates adipisci et eaque veritatis ipsam. Sit aut non animi repellendus iste nemo.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Pewter Caster'),
(50, 'Prof.', 'Doloribus autem alias beatae impedit error dolore. Expedita esse voluptas sit aspernatur iusto minima. Ducimus cupiditate facilis voluptas nam voluptas consequatur.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Mixing and Blending Machine Operator'),
(51, 'Miss', 'Atque deserunt natus vero non ducimus distinctio repellat. Quia perspiciatis omnis aliquam et perspiciatis ea. Ipsa sunt qui incidunt excepturi. Sed aut adipisci error exercitationem.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Geographer'),
(52, 'Ms.', 'Repudiandae amet omnis accusamus ipsa deserunt autem. Quibusdam et similique sed non. Occaecati molestiae vel ipsum nostrum rerum. Voluptatibus quibusdam ut asperiores molestias. Sed est saepe sunt natus esse.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Home Health Aide'),
(53, 'Dr.', 'Ea distinctio sed omnis. Dolorum quia adipisci in laborum consectetur. Soluta et sed inventore dignissimos consectetur soluta. Sunt et laboriosam asperiores harum. Debitis magni in illum omnis dignissimos minima.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Dot Etcher'),
(54, 'Prof.', 'Voluptatem accusantium eos quibusdam sit rerum velit. Odit natus facilis dolores. Omnis voluptate aut deserunt qui eius totam quis.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Airframe Mechanic'),
(55, 'Miss', 'Ut adipisci rerum repellat. Ad est laborum commodi necessitatibus autem aperiam. Iure blanditiis officiis dolorem voluptatem omnis. Minus quia possimus odit necessitatibus voluptatem laborum.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Upholsterer'),
(56, 'Miss', 'In doloremque voluptatum modi nostrum aut iusto et. Nihil dolor eos omnis et. Cupiditate molestiae facilis praesentium atque nihil dolores. Vero inventore nisi quia at nihil voluptatem.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Sys Admin'),
(57, 'Mr.', 'Rerum non in sed corrupti. Est ea fugiat ullam et et. In ut eos cupiditate quia voluptatem quae possimus. Ipsum id voluptas ut sit id est reiciendis velit.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Multi-Media Artist'),
(58, 'Dr.', 'Mollitia omnis necessitatibus qui ea aut rem quis. Inventore quam occaecati explicabo nihil eius animi. Ut excepturi dolor velit itaque occaecati et. Corporis sunt sed eveniet dolor qui sequi dolores quia.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Bellhop'),
(59, 'Mr.', 'Qui et eos voluptas minus error illo. Pariatur non maxime illum tempore aut. Voluptatem voluptatem non et aut.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Production Laborer'),
(60, 'Ms.', 'Voluptate itaque explicabo eligendi ut velit amet inventore dolorem. Excepturi est ipsum aspernatur labore et voluptatem aut. Aperiam corrupti amet quibusdam incidunt.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Artist'),
(61, 'Dr.', 'Similique officiis et distinctio ut magnam id neque. Assumenda qui fuga blanditiis omnis omnis aliquam voluptatibus. Qui accusantium illo doloremque consequuntur ut aut. Vel odio doloribus maxime voluptatem repellat. Cum optio eos optio sed reiciendis sed ut.', '2020-09-16 03:01:53', '2020-09-16 03:01:53', NULL, 'Arbitrator'),
(62, 'Miss', 'Ea et dolores corporis molestiae et. Sed eligendi blanditiis quam ut soluta cum consequuntur repellendus. Et voluptatem repudiandae fugit deserunt quia totam molestiae. Dolor atque amet dolor in eius. Nihil ut sunt officia itaque qui reprehenderit.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Metal Fabricator'),
(63, 'Miss', 'Eveniet et maxime porro reiciendis. Sunt in inventore doloremque. Incidunt labore vel minus. Ut reprehenderit dolores enim laudantium praesentium vitae. Nam rem consequuntur quia aut illum velit.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Welfare Eligibility Clerk'),
(64, 'Prof.', 'Veniam tempora perspiciatis qui ex quia facere sed. Qui omnis consequatur temporibus dolor sit. Consequatur praesentium dicta qui reprehenderit. Minus deserunt ea repellendus consequatur nobis ab.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Carver'),
(65, 'Dr.', 'Illo eius aut labore cumque libero. Fuga id fugit consequatur non tempora occaecati tempora voluptatem.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Environmental Engineering Technician'),
(66, 'Dr.', 'Fugit iste nobis cupiditate modi modi amet maiores. Optio fugit velit tempore. Dignissimos animi temporibus ut quo. Et mollitia cum qui facere sequi iure error. Autem perferendis inventore placeat temporibus.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Camera Repairer'),
(67, 'Dr.', 'Neque aperiam eius minus perferendis accusantium. Ipsam ullam labore qui qui molestiae ratione voluptatem est.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Welder and Cutter'),
(68, 'Ms.', 'Magnam beatae excepturi eum consectetur quasi et. Ut repudiandae eligendi veritatis sed. Consequatur quo velit id.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Gas Pumping Station Operator'),
(69, 'Ms.', 'Perferendis ab voluptas eum expedita laborum voluptatem sed. Iure et ratione vel quaerat facere perferendis et. Quisquam molestias sequi quasi et quis quis. Hic vero earum repudiandae omnis dolorem accusamus ut.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Foundry Mold and Coremaker'),
(70, 'Mr.', 'Modi totam qui dolorem corporis nemo doloremque. Commodi quia asperiores et quia provident. Nihil vel eos cupiditate. Praesentium quam commodi qui consequatur hic velit et. Eligendi aut autem animi nesciunt dolores doloribus possimus.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Agricultural Worker'),
(71, 'Dr.', 'Non ut voluptas ex incidunt minus corrupti fugit. Est fugiat ab non soluta architecto occaecati voluptatem. Est molestias corrupti et non molestiae. Officia ipsam iste excepturi corrupti.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Record Clerk'),
(72, 'Dr.', 'Et eius similique eius rerum veritatis alias sunt qui. Veniam veritatis ut et et est rerum nemo.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Storage Manager OR Distribution Manager'),
(73, 'Dr.', 'Facere qui quis cumque sunt. Possimus totam suscipit nihil ut animi. Maiores odio odit error sit debitis aut rerum. Sed rerum officia rerum rerum qui totam.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Chef'),
(74, 'Mr.', 'Eius reprehenderit expedita consequatur nostrum quo eius. Beatae perferendis qui debitis. Esse dolore error voluptates quaerat est.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Materials Engineer'),
(75, 'Prof.', 'Inventore beatae neque reiciendis omnis iusto. Molestias aut fugit vero qui itaque neque fugit repudiandae. Omnis velit sint autem voluptatum porro et vitae.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Ceiling Tile Installer'),
(76, 'Mr.', 'Soluta quo maxime molestias dolor delectus nam. Ab commodi dolorem aut quia cupiditate. Possimus animi rerum blanditiis quia.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Transportation Equipment Painters'),
(77, 'Mr.', 'Officia cumque ut et quam quos rerum. Expedita illum nesciunt reprehenderit aspernatur vitae iste fugit nostrum. Quas est libero nam vel ex. Voluptatem et reprehenderit necessitatibus quo provident.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Agricultural Equipment Operator'),
(78, 'Mr.', 'Ut aut non et. Similique eum incidunt repudiandae aut rerum rem nesciunt consectetur. Suscipit eligendi culpa minus laboriosam ipsam fuga. Aspernatur voluptas maiores voluptas aut minima quis earum.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Space Sciences Teacher'),
(79, 'Prof.', 'Quidem consequatur illo qui totam aliquid sint et. Qui consequatur corrupti dolores voluptatem impedit. Alias inventore facilis est fugiat ex libero.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Truck Driver'),
(80, 'Dr.', 'Ut eos ducimus quia ut non provident. Ut sed est est eaque at odio asperiores.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Webmaster'),
(81, 'Mr.', 'Quam sint sed non id laudantium minus. Velit omnis debitis libero ad impedit consequatur amet. Aut qui dicta aut. Voluptatum dignissimos cum est consequatur esse.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Watch Repairer'),
(82, 'Mr.', 'Deserunt veritatis et aut et laborum. Fuga voluptas nemo voluptas. Excepturi numquam incidunt magni repellendus quisquam quo vero.', '2020-09-16 03:01:54', '2020-09-16 03:01:54', NULL, 'Construction Manager'),
(83, 'Mr.', 'Minima rerum eum ea similique adipisci. Consectetur ratione nesciunt ratione nulla qui. Autem id et porro. Itaque expedita consequuntur quam.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Automotive Body Repairer'),
(84, 'Mrs.', 'Qui illum sunt ipsam sed. Maxime ea animi adipisci corporis. Voluptatum architecto error ipsa totam cum dolorem consequatur. Quas officiis inventore maiores neque.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Photographic Restorer'),
(85, 'Prof.', 'Qui et dicta ab. Omnis accusamus beatae officiis consequatur molestiae non quidem. Ipsa est laudantium ea tempore ut. Nesciunt et voluptates sit impedit quia delectus.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Municipal Clerk'),
(86, 'Mr.', 'Officiis vel iusto eaque reiciendis. Quia et quibusdam occaecati saepe quis rerum. Delectus sit voluptas incidunt quis sit culpa non.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Production Manager'),
(87, 'Mr.', 'Quibusdam dolor suscipit eveniet id ab nobis quia libero. Sit earum ad quo vero voluptas. Et voluptatem iure beatae nesciunt et fugit.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Slot Key Person'),
(88, 'Prof.', 'Nemo voluptatum facere quos eveniet perferendis quod. Atque illo quam nostrum velit sit et dolor. Ratione et ipsum praesentium non.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Bartender Helper'),
(89, 'Miss', 'Facilis eligendi cum ducimus magnam modi. Itaque necessitatibus est veniam consequatur. Asperiores alias labore et aut omnis dolor iusto assumenda. Deleniti ea cupiditate dolore dolorem itaque sunt.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Costume Attendant'),
(90, 'Mrs.', 'Laudantium sunt odio soluta architecto vitae ipsum quo omnis. Et amet nihil eaque aut voluptate. Suscipit distinctio et tempora laudantium. Illo et repellat facere provident.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Internist'),
(91, 'Mr.', 'Dolor ut doloremque consequatur qui omnis. Commodi aut id dolorem aspernatur. Officiis molestias quidem ut quae qui quaerat tenetur. Est quos officia maiores exercitationem.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Parking Enforcement Worker'),
(92, 'Mr.', 'Sit sapiente aliquam officiis qui suscipit rerum dolorem. Veniam ea aperiam eos aliquid soluta dolorum labore sunt. Saepe possimus aliquid provident.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Architect'),
(93, 'Prof.', 'Consectetur eaque delectus quis ut autem quibusdam. Qui soluta fuga ratione eligendi modi officiis. Dolor et ducimus tempora dolores aliquid labore. Cumque commodi veniam nulla tenetur sunt qui porro. Itaque beatae eius asperiores totam aliquam voluptas ipsam a.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Grinder OR Polisher'),
(94, 'Mrs.', 'Iusto maxime sequi aspernatur incidunt hic. Voluptatem ut quis aut eaque. Minus expedita deserunt tempore porro ut eaque.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Travel Guide'),
(95, 'Dr.', 'Id quo et et sint quas aut. Aut ab tenetur officiis ipsa rem laudantium voluptatibus voluptatem. Eligendi unde vel et itaque deserunt in autem dolorem. Veritatis aut commodi consequuntur et.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Health Educator'),
(96, 'Dr.', 'Enim repellendus deleniti ea et labore magni dolor expedita. Recusandae sed qui omnis omnis eveniet.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Biochemist or Biophysicist'),
(97, 'Mr.', 'Et non nostrum distinctio ipsa iusto saepe quia. Impedit impedit et quia eos nisi excepturi id. Et perferendis veritatis deserunt beatae consectetur. Quis praesentium qui reprehenderit quos maiores iusto.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Electrical Engineer'),
(98, 'Mrs.', 'Blanditiis et est rerum ipsa libero possimus asperiores. Non dignissimos ex necessitatibus ut ex ducimus. Aspernatur rem et est inventore.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Photoengraver'),
(99, 'Dr.', 'Eum deserunt quam tempore porro et eum voluptas. Saepe aperiam nostrum qui in sed accusantium. Voluptas quo suscipit nihil.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Radio and Television Announcer'),
(100, 'Ms.', 'Et temporibus culpa aspernatur laudantium. Adipisci id quae distinctio et. Cum id accusamus officia saepe aut quo occaecati.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Radiation Therapist'),
(101, 'Dr.', 'Modi quaerat rerum vel dolor necessitatibus fugiat. Laudantium sed dolorem sit eum officiis natus. Doloremque deserunt labore nulla sed similique et.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Gaming Supervisor'),
(102, 'Ms.', 'Sit tenetur aut quae unde. Dolor dolor et iure consequuntur qui. Voluptatibus aperiam officia ullam voluptas dicta ratione. Recusandae error sint possimus rerum ut fugiat omnis est.', '2020-09-16 03:01:55', '2020-09-16 03:01:55', NULL, 'Paperhanger');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2020_09_16_085045_create_blog_table', 1),
(6, '2020_09_16_085718_add_category_in_blog_table', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
